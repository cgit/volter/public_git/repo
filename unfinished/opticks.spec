Name:           opticks
Version:        4.5.0
Release:        1%{?dist}
Summary:        Opticks is an expandable remote sensing and imagery analysis software platform

Group:          Applications/Scientific
License:        LGPLv2.1
URL:            http://www.opticks.org
# No tarballs available -- 4.5 = r11733
# http://opticks.svn.sourceforge.net/viewvc/opticks/trunk/4.5.0/Code/?view=tar
# http://opticks.svn.sourceforge.net/viewvc/opticks/trunk/4.5.0/Release/?view=tar
Source0:        

BuildRequires:  scons
BuildRequires:  raptor
BuildRequires:  expat
BuildRequires:  doxygen graphviz
BuildRequires:  curl
BuildRequires:  ffmpeg
BuildRequires:  gdal proj shapelib
#libgeotiff -- Sollte von gdal her da sein -- Bzw. macht das xqilla, das auch pthreads haben dürfte
# xqilla und shapelib sind ev. nur unter Windows nötig
BuildRequires:  xqilla  
BuildRequires:  zlib
BuildRequires:  glew
BuildRequires:  ossim
BuildRequires:  minizip
BuildRequires:  yaml-cpp
BuildRequires:  pcre
# Dieses cg-Zeug ist von NVIDIA
# GDAL mit Xerces? Könnte bei EL vielleicht interessant sein
# Von EHS scheinen sie eine Fork zu haben. http://xaxxon.slackworks.com/ehs/ -- Jedenfalls ist das Zeug offensichtlich tot. Oh, gar nicht: http://ehs.fritz-elfert.de/
# Warum benötigt GDAL 1.7.3. nicht das Paket xerces?
Requires:       Qt

%description


%prep
%setup -q


%build
python build.py --build-doxygen=all
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc



%changelog
