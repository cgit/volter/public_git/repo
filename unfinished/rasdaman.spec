Name:           rasdaman
#Unfug
Version:        8.0.0 
Release:        1%{?dist}
Summary:        Weiss nicht

Group:          System Environment/Libraries
License:        GPLv3+
URL:            http://www.rasdaman.org
Source0:        %{name}-%{version}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: hdf-devel hdf-static  
BuildRequires: netpbm-devel
BuildRequires: libtiff-devel
# Ist die jetzt aktuell?
BuildRequires: libjpeg-devel
BuildRequires: libpng-devel
# Java 8.3.0 bis 8.3.6 gehen nicht
BuildRequires: postgresql-devel
#Java shit
BuildRequires: openssl-devel
Requires: hdf
Requires(pre): /usr/sbin/useradd

%description
Dies und das

%package devel
Summary:        Development Libraries for %{name} 
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Provides:       %{name}-static = %{version}-%{release}

%description devel
Development package for %{name} including header files.


%prep
%setup -q

#Syntax error
#sed -i 's| -fPIC|\" -fPIC\"|' configure

#GCC-4.5-Problem
#sed -i 's|^QtONCStream::QtONCStream\*$|QtONCStream\*|' qlparser/qtdelete.cc

%build

export CPPFLAGS="$CPPFLAGS -I%{_includedir}/netpbm"
export LDFLAGS="$LDFLAGS -L%{_libdir}/hdf"

# Some header names are just to generic to let them go directly into /usr/include
%configure \
    --with-logdir=%{_var}/log/%{name} \
    --includedir=%{_includedir}/%{name}

make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%pre
# Add "rasdaman" user
/usr/sbin/useradd -c "rasdaman" #-u? und mehr

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc COPYING
%config(noreplace)%{_sysconfdir}/rasmgr.conf
%{_bindir}/*
%{_datadir}/%{name}
%{_var}/log/%{name}


%files devel
%defattr(-,root,root,-)
%{_includedir}/%{name}
%{_libdir}/*

%changelog
* Tue Mar 01 2011 Volker Fröhlich <volker27@gmx.at> - 8.0.0-1
- Initial packaging

