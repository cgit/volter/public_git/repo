Name:      libgeotiff
Version:   1.3.0
Release:   4%{?dist}
Summary:   GeoTIFF format library
Group:     System Environment/Libraries
License:   MIT
# The license for the EPSG-files changed, but was forgotten to change in the LICENSE file.
# See http://www.epsg-registry.org/help/xml/Terms_Of_Use.html
URL:       http://trac.osgeo.org/geotiff/
Source0:   http://download.osgeo.org/geotiff/%{name}/%{name}-%{version}.tar.gz
Source1:   %{name}-docs.tar.gz
# The files for doxygen were pulled from upstream's vcs, since they were not included.
# Use the following commands to generate the tarball:
#  svn export -r 1731 https://svn.osgeo.org/metacrs/geotiff/trunk/libgeotiff/Doxyfile
#  svn export -r 1731 https://svn.osgeo.org/metacrs/geotiff/trunk/libgeotiff/docs
#  tar cvfz libgeotiff-docs.tar.gz docs Doxyfile
BuildRequires: doxygen graphviz
BuildRequires: chrpath
BuildRequires: libtiff-devel libjpeg-devel proj-devel zlib-devel

%description
GeoTIFF represents an effort by over 160 different remote sensing, 
GIS, cartographic, and surveying related companies and organizations 
to establish a TIFF based interchange format for georeferenced 
raster imagery.

%package devel
Summary: Development Libraries for the GeoTIFF file format library
Group: Development/Libraries
Requires: pkgconfig libtiff-devel
Requires: %{name} = %{version}-%{release}

%description devel
The GeoTIFF library provides support for development of geotiff image format.

%prep
%setup -q

# Extract missing files for doxygen
tar xvfz %{SOURCE1}

# Change encoding for some csv-files
for f in `find csv -type f | grep -q ISO-8859-1` ; do
      iconv -f ISO-8859-1 -t UTF-8 $f > ${f}.tmp
      touch -r $f $f.tmp
      mv -f ${f}.tmp $f
done

# Correct permissions
chmod 644 csv/ellipsoid.csv

# Remove custom compiler option
sed -i 's/ -O3 / /' configure

%build

%configure \
        --prefix=%{_prefix} \
        --includedir=%{_includedir}/%{name}/ \
        --with-proj=%{_prefix}               \
        --with-jpeg \
        --with-zip \
        --disable-static \
        --enable-doxygen-man=NO

##  WARNING
##  disable %{?_smp_mflags}
##  it breaks compile

# Can't prove that true.
make %{?_smp_mflags}
make docs

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

# Install pkgconfig file
cat > %{name}.pc <<EOF
prefix=%{_prefix}
exec_prefix=%{_prefix}
libdir=%{_libdir}
includedir=%{_includedir}/%{name}

Name: %{name}
Description: GeoTIFF file format library
Version: %{version}
Libs: -L\${libdir} -lgeotiff
Cflags: -I\${includedir}
EOF

mkdir -p %{buildroot}%{_libdir}/pkgconfig/
install -p -m 644 %{name}.pc %{buildroot}%{_libdir}/pkgconfig/

# Remove unnecessary empty file -- already removed from Makefile and physically for next release 
rm -f %{buildroot}%{_datadir}/epsg_csv/codes.csv

# Remove libtool archive
rm -f %{buildroot}/%{_libdir}/%{name}.la

# Remove rpath from files
chrpath --delete \
    %{buildroot}%{_bindir}/csv2html \
    %{buildroot}%{_bindir}/listgeo \
    %{buildroot}%{_bindir}/applygeo \
    %{buildroot}%{_bindir}/makegeo \
    %{buildroot}%{_bindir}/geotifcp

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files 
%defattr(-,root,root,-)
%doc ChangeLog LICENSE README
%doc docs/*.txt docs/*.html
%{_bindir}/geotifcp
%{_bindir}/applygeo
%{_bindir}/csv2html
%{_bindir}/listgeo
%{_bindir}/makegeo
%{_libdir}/%{name}.so.*
%{_datadir}/epsg_csv

%files devel
%defattr(-,root,root,-)
%doc docs/output/html
%{_includedir}/%{name}
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/%{name}.pc

%changelog
* Sat Aug 14 2010 Volker Fröhlich <volker27@gmx.at> - 1.3.0-4
- Removed custom compiler option

* Fri Aug 13 2010 Volker Fröhlich <volker27@gmx.at> - 1.3.0-3
- Removed -r option from rm, where not needed
- Turned off creation of manpages for API, since HTML is produced
- Dropped obsolete sed manipulation for Makefile.in and configure

* Wed Aug 11 2010 Volker Fröhlich <volker27@gmx.at> - 1.3.0-2
- Changed license back to MIT 
- Altered the commands to change permissions and encoding for csv-files
- Unified the use of RPM_BUILD_ROOT and buildroot
- Removed name parameters from setup-macro
- Corrected SVN revision number for docs tarball
- Delete the libtool archive and disable creation of static library
- Added -rf to the csv-file remove command

* Thu Aug 05 2010 Volker Fröhlich <volker27@gmx.at> - 1.3.0-1
- Updated for version 1.3.0
- Dropped obsolete patches 
- Added docs tarball and create documentation with make
- Removed unnecessary carriage return correction
- Dropped obsolete rm command for .cvsignore
- Delete unnecessary empty file codes.csv (Confirmed by Frank Warmerdam)
- Re-enabled smp_flags
- Removed configure parameter with-tiff, since it's default
- Simplified make install call and dropped BuildRoot definition
- Shifted permission handling away from files-section

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.5-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Apr 22 2009 Milos Jakubicek <xjakub@fi.muni.cz> - 1.2.5-4
- Fix FTBFS: use gcc -shared instead of ld -shared to compile with -fstack-protector

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.5-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Sep 15 2008 Balint Cristian <rezso@rdsor.ro> - 1.2.5-2
- disable smp build for koji

* Mon Sep 15 2008 Balint Cristian <rezso@rdsor.ro> - 1.2.5-1
- new bugfix release

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.2.4-3
- Autorebuild for GCC 4.3

* Sun Jan 06 2008 Balint Cristian <rezso@rdsor.ro> - 1.2.4-2
- Fix multilib issue by removal of datetime in doxygen footers

* Sun Jan 06 2008 Balint Cristian <rezso@rdsor.ro> - 1.2.4-1
- Rebuild for final release.

* Wed Aug 29 2007 Fedora Release Engineering <rel-eng at fedoraproject dot org> - 1.2.4-0.5.rc1
- Rebuild for selinux ppc32 issue.

* Wed Jul 25 2007 Jesse Keating <jkeating@redhat.com> - 1.2.4-0.4.rc1
- Rebuild for RH #249435

* Tue Jul 24 2007 Balint Cristian <cbalint@redhat.com> 1.2.4-0.3.rc1
- codes are under MIT
- pkg-config cflags return fix
- epsg_csv ownership

* Mon Jul 23 2007 Balint Cristian <cbalint@redhat.com> 1.2.4-0.2.rc1
- fix debuginfo usability
- move header files to the subdirectory
- specify the full URL of the source
- leave *.inc headers included
- libgeotiff-devel should require libtiff-devel
- works to keep timestamps on the header files installed
- docs proper triage

* Mon Jul 23 2007 Balint Cristian <cbalint@redhat.com> 1.2.4-0.1.rc1
- initial pack for fedora
- add pkgconfig file
- add soname versioning patch
