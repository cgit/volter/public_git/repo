Name:      librasterlite
Version:   1.0
Release:   1%{?dist}
Summary:   Support Raster Data Sources within a SpatiaLite DB
Group:     System Environment/Libraries
License:   GPLv3
URL:       http://www.gaia-gis.it/spatialite
Source0:   http://www.gaia-gis.it/spatialite/librasterlite-1.0.tar.gz

BuildRequires: proj-devel geos-devel sqlite-devel
BuildRequires: libtiff-devel
BuildRequires: libgeotiff-devel
BuildRequires: libjpeg-devel
BuildRequires: zlib-devel
BuildRequires: libpng-devel
BuildRequires: libepsilon-devel
BuildRequires: libspatialite-devel
#BuildRequires: proj-devel


%description
A library supporting Raster Data Sources within a SpatiaLite DB
and any releated CLI management tool.

%package devel
#Quatsch
Summary: Development Libraries for the SpatiaLite extension
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The RasterLite library provides support for spatial data in SQLite.
#Quatsch

%prep
%setup -q

rm -rf epsilon
rm -f Makefile-static*
sed -i 's/epsilon//g' Makefile.in
sed -i 's/epsilon\/Makefile//g' configure
sed -i 's/spatialite\/sqlite3.h/sqlite3.h/g' lib/*.c
sed -i 's/spatialite\/sqlite3.h/sqlite3.h/g' src/*.c


%build
%configure --prefix=%{_prefix} \
            LDFLAGS="-Lepsilon" \
            CPPFLAGS="%{_includedir}/libgeotiff"

make %{?_smp_mflags}
i#C_INCLUDE_PATH=%{_includedir}/libgeotiff


%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}


%clean
rm -rf %{buildroot}


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files 
%defattr(-,root,root,-)
%doc COPYING AUTHORS

%files devel
%defattr(-,root,root,-)


%changelog
* Sun Nov 28 2010 Volker Fröhlich <volker27@gmx.at> 1.0-1
- Initial packaging for Fedora
