Name:           mod_geocache
Version:        0.2.1
Release:        1%{?dist}
Summary:        Tile caching module for the Apache HTTP Server

Group:          Development/Tools
License:        ASL 2.0
#Sonderbehandlung
Source0:        http://mod-geocache.googlecode.com/files/mod-geocache-%{version}.tar.gz
URL:            http://code.google.com/p/mod-geocache/
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildRequires:  httpd-devel
BuildRequires:  libcurl-devel
BuildRequires:  libxml2-devel
BuildRequires:  libpng-devel
BuildRequires:  libjpeg-devel
BuildRequires:  apr-devel
BuildRequires:  gdal-devel

%description
mod-geocache aims to implement a subset of the features offered by existing
tile caching solutions. The primary objectives are to be fast and easily 
deployable, while offering the essential features expected from a
tile caching solution. 

#Eventuell Teilung in base fcgi und mod

%prep
%setup -q -n mod-geocache-%{version}


%build
%configure --with-fastcgi=%{_prefix}
make %{?_smp_mflags}
#Das Modul aktiviert sich selbst. Ist das OK?
%install
rm -rf %{buildroot}
install -Dpm 755 src/.libs/mod_geocache.so %{buildroot}/%{_libdir}/httpd/modules/mod_geocache.so
#Flags? in Makefile.inc sind die
#Installationspfade statisch?
#make install-module
#make install-fcgi
#doxygen


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc LICENSE
%{_libdir}/httpd/modules/mod_geocache.so

%changelog
* Sat Jan 22 2011 Volker Fröhlich <volker27@gmx.at> - 0.2.1-1
- Inital packaging
