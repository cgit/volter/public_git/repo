Name:           orfeo
Version:        3.8.0        
Release:        1%{?dist}
Summary:        Library of image processing algorithms

Group:          Applications/Engineering
#prüfen
License:        CeCILL
URL:            http://www.orfeo-toolbox.org/otb/
#korrigieren
Source0:        http://sourceforge.net/projects/orfeo-toolbox/files/orfeo-toolbox/OTB-3.8/OrfeoToolbox-3.8.0.tgz
#epel
#/download 

#ming
BuildRequires: gdal-devel 
BuildRequires: fltk
BuildRequires: automake

%description
ORFEO Toolbox is a library of image processing algorithms. OTB is based on the
medical image processing library ITK and offers particular functionalities for
remote sensing image processing in general and for high spatial resolution
images in particular.

%package devel
Summary:        Development Libraries for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
Development package for %{name}
%prep
%setup -qn OrfeoToolbox-%{version}


mkdir sau

%build
#lib ist noch
cd sau
%cmake \
    -DCMAKE_INSTALL_PREFIX:PATH=%{_prefix} \
    -DOTB_USE_EXTERNAL_FLTK:BOOL=ON \
    ..
#Internes Fltk, Jpeg2000, ITK, liblas, boost, tinyxml, openmp
#Bei GDAL werden die Rasterformate nicht erkannt. An sich sollen die Tests aber laufen. Aus irgendeinem Grund sind auch die INCLUDE_DIRECTORIES manchmale auskommentiert -- was auch immer das bringt! OpenGL-Tand findet er auch nicht
#Mapnik, pqxx inaktiv

make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd sau
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc RELEASE_NOTES.txt
#Keine Lizenz dabei!

%{_libdir}/otb
%{_bindir}

%files devel
%defattr(-,root,root,-)
%{_includedir}/otb

%changelog
* Thu Feb 03 2011 Volker Fröhlich <volker27@gmx.at> - 3.8.0-1
- Initial packaging
