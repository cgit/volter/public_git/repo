Name:           openmodeller-desktop
Version:        1.1.0
Release:        1.20110127svn%{?dist}
Summary:        Desktop application for openmodeller

Group:          Desktop
License:        GPLv2+
URL:            http://openmodeller.sourceforge.net
#Gehört noch bearbeitet
#Source0:        http://sourceforge.net/projects/openmodeller/files/openModeller%20Desktop/1.1.0/openModellerDesktop-src-%{version}.tar.bz2
Source0:        openmodeller-desktop-svn5259.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  openssl-devel

#Requires:       

%description
Bla.

%package devel
Summary:        Development Libraries for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
Development packages for %{name}.


%prep
%setup -qn %{name}-svn5259


%build

%cmake -DCMAKE_INSTALL_PREFIX:PATH=%{_prefix} \
    -DQGIS_PLUGIN_DIR:PATH=/usr/share/qgis/python/plugins \
    -DCMAKE_SKIP_RPATH:BOOL=ON \
    -DGDAL_LIBRARY:PATH=%{_libdir}/libgdal.so \
    -DOM_LIBRARY:PATH=%{_libdir}/libopenmodeller.so \
    -DOM_LIB_DIR:PATH=%{_libdir}/libopenmodeller \
    -DOM_INCLUDE_DIR:STRING=%{_includedir}/openmodeller \
    -DQWT_INCLUDE_DIR:PATH=%{_includedir}/qwt \
    -DWITH_DESIGNER:BOOL=ON
    #-DWITH_QGIS:BOOL=OFF


#make %{?_smp_mflags}
make

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%find_lang OpenModellerDesktop --with-qt --all-name
#//preferred path to GDAL (gdal-config)
#GDAL_CONFIG_PREFER_PATH:STRING=/bin
#Om-Library-Pfad wird falsch erkannt
#qgis_plugin_dir
#LIB_SUFFIX
#qgis-bibpfade auch in lib
#svn-marker
#with_designer
#Doxygen
#qwt und gdal, python, ossim -- was ist webcatalogue und experimental?
#Tests
#Desktop file
%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

#%files -f %{name}.lang
%files
%defattr(-,root,root,-)
%doc LICENSE.txt TODO CODING_GUIDE.txt
#contributors wahrscheinlich unnötig; changelog.html auch
%{_bindir}/*
#%{_libdir}/libomg*
#%{_libdir}/openModellerDesktop


%files devel
%defattr(-,root,root,-)
#%{_includedir}/OpenModellerDesktop

%changelog

* Thu Jan 27 2011 Volker Fröhlich <volker27@gmx.at> - 1.1.0-1.20110127svn
- Initial package
