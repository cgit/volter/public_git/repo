%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%endif
# Can be removed after EOL of F12

Name:           pyspatialite
Version:        2.6.1
Release:        1%{?dist}
Summary:        Wos was i

Group:          Development/Libraries
License:        zlib
URL:            http://trac.gispython.org/lab/wiki/Shapely
#http://code.google.com/p/pyspatialite/downloads/detail?name=pyspatialite-2.6.1.tar.gz&can=2&q=
Source0:        %{name}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python-devel python-setuptools-devel

%description
bla.

%prep
%setup -q
#Probleme mit integriertem Sqlite
#Downloading amalgation -- aber von Spatialite scheinbar.
#Da wird auch wirklich was von Sqlite (!) kompiliert.

%build
%{__python} setup.py build


%check
#%{__python} setup.py test


%install
rm -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc LICENSE
%doc PKG-INFO doc
%{python_sitelib}/%{name}
%{python_sitelib}/%{name}-%{version}-py*.egg-info


%changelog
* Tue Jan 18 2011 Volker Fröhlich <volker27@gmx.at> - 2.6.1-1 
- Initial package for Fedora
