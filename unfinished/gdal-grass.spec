%define grasspath %{_libdir}/grass-6.3.0

Name:		gdal-grass
Version:	1.4.3
Release:	3%{?dist}
Summary:	Standalone GRASS 6 Drivers for GDAL and OGR

Group:		Applications/Engineering
License:	MIT
URL:		http://www.gdal.org/
Source0:	http://download.osgeo.org/gdal/%{name}-%{version}.tar.gz
#Patch0:		gdal-grass-1.4.3-ldflags.patch
#Patch1:		gdal-grass-1.4.3-prefix.patch
Patch2:		gdal-grass-1.4.3-ln.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: grass
BuildRequires: grass-devel
BuildRequires: proj-devel
BuildRequires: zlib-devel
BuildRequires: gdal-devel

%description
This package contains standalone drivers for GRASS raster and vector
files that can be built after GDAL has been built and installed as an
"autoload" driver.

This is particularly useful in resolving problems with GRASS depending
on GDAL, but GDAL with GRASS support depending on GRASS.  With this
package you can configure and install GDAL normally (--without-grass), then
build and install GRASS normally and finally build and install this driver.

%prep
%setup -q
#%patch0 -p0 -b .ldflags 
#%patch1 -p0 -b .prefix
%patch2 -p1 -b .ln

%build
%configure	\
	--enable-shared					\
	--with-grass=%{grasspath}			\
	--with-autoload=%{_libdir}/%{name}/plugins	\
	--with-gdal=%{_bindir}/gdal-config-64
    #TODO: Achtung 64

make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# make plugin available on system
install -d %{buildroot}/etc/ld.so.conf.d
install -d %{buildroot}/etc/profile.d
echo "%{_libdir}/gdal-grass/plugins" > %{buildroot}/%{_sysconfdir}/ld.so.conf.d/gdal-grass.conf
echo "export GDAL_DRIVER_PATH=%{_libdir}/gdal-grass/plugins" > %{buildroot}/%{_sysconfdir}/profile.d/gdal-grass.sh

%clean
rm -rf %{buildroot}

%post 
/sbin/ldconfig
echo -e "\e[0;34mPlease \e[0;31mlogout and login \e[0;34magain or \
run \e[0;31mexport GDAL_DRIVER_PATH=%{_libdir}/gdal-grass/plugins \
\e[0;34mto set the plugin path. To verify the installation run \
\e[0;31mgdal_translate --formats | grep -i grass. \e[0;34mThis should \
return something similar to \e[0;31mGRASS (ro): GRASS Database Rasters \
(5.7+)\e[0m"

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_libdir}/%{name}
%{_sysconfdir}/ld.so.conf.d/gdal-grass.conf
%{_sysconfdir}/profile.d/gdal-grass.sh

%changelog
* Thu Mar 31 2011 Volker Fröhlich <volker27@gmx.at> - 1.4.3-3
- Removed implicit BRs and Rs
- Use symlinks instead of copying files from GRASS

* Sat Dec 04 2010 Viji Nair <viji [AT] fedoraproject DOT org> - 1.4.3-2
- Updated the description
- Dropped unwanted BRs
- Removed %%doc, no specific documentation available
- Used macros instead of some static paths
- Used Wiledcard syntax to avoid version conflicts

* Wed Nov 24 2010 Viji Nair <viji [AT] fedoraproject DOT org> - 1.4.3-1
-Initial Build
