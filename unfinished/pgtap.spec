%global postgresver %(pg_config --version|awk '{print $2}'| cut -d. -f1,2)

Name:		pgtap
Version:	0.25.0
Release:	1%{?dist}
Summary:	Unit testing suite for PostgreSQL

Group:		Applications/Databases
License:	BSD
URL:		http://pgtap.org
Source:     http://pgfoundry.org/frs/download.php/2937/%{name}-%{version}.tar.bz2

BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildRequires:	postgresql-devel
#Auch entfernt einsetzbar
#Aber für 8.3 und kleiner braucht man die .so auf dem Server; if?
#Requires:       postgresql-server
#Eventuell perl-TAP-Harness in EPEL
#Eigentlich wollen wir aber http://search.cpan.org/dist/TAP-Parser-SourceHandler-pgTAP/lib/TAP/Parser/SourceHandler/pgTAP.pm
#Gibt es unter dem Namen nur in F
#Perl braucht man sicher, aber ob man das wirklich braucht?

# Test Harness is a replacement for TAP Harness
#%if %{?fedora}
#  Requires: perl-Test-Harness
#%else
#  Requires: perl-TAP-Harness
#%endif

# PG 8.4 and up have the necessary functions inside the core
%if "%{postgresver}" >= "8.4"
BuildArch:	noarch
%endif
 
%description
pgTAP is a unit testing suite for PostgreSQL written in PL/pgSQL and
PL/SQL. It includes a comprehensive collection of TAP-emitting assertion
functions, as well as the ability to integrate with other TAP-emitting
test frameworks. It can also be used in the xUnit testing style.

 
%prep
%setup -q


%build
make USE_PGXS=1 TAPSCHEMA=tap


%install
rm -rf %{buildroot}
make install USE_PGXS=1 DESTDIR=%{buildroot}


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc
%if "%{postgresver}" < "8.4"
%{_libdir}/pgsql/pgtap.so
%endif
%{_datadir}/pgsql/contrib/*
%{_docdir}/pgsql/contrib/%{name}.md


%changelog
* Thu Feb 24 2011 Volker Fröhlich <vokler27@gmx.at> 0.25.0-1
- Adapted for Fedora

* Tue Feb 01 2011 David Wheeler <david@kineticode.com> 0.25.0
- Removed pg_prove and pg_tapgen, which are now distributed via CPAN.

* Sun Mar 01 2010 Darrell Fuhriman <darrell@renewfund.com> 0.24-2
- Make install work where the pgtap.so library is needed.

* Sun Dec 27 2009 David Wheeler <david@kineticode.com> 0.24-1
- Updated Source URL to a more predictable format.

* Mon Aug 24 2009 David Fetter <david.fetter@pgexperts.com> 0.23-1
- Got corrected .spec from Devrim GUNDUZ <devrim@gunduz.org>
- Bumped version to 0.23.

* Wed Aug 19 2009 Darrell Fuhriman <darrell@projectdx.com> 0.22-1
- initial RPM
 
