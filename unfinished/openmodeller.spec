%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%endif

Name:           openmodeller
Version:        1.1.0
Release:        1%{?dist}
Summary:        Fundamental niche modeling library

Group:          System Environment/Libraries
License:        GPLv2+
URL:            http://openmodeller.sourceforge.net/ 
Source0:        http://downloads.sourceforge.net/%{name}/lib%{name}-src-%{version}.tar.bz2
#Cmake and Python script taken from QGIS
Source1:        FindPythonLibs.cmake
Source2:        FindLibPython.py

#Taken from http://wush.net/trac/sehenley/browser/simulator/trunk/vendor/cmake/Modules/FindSWIG.cmake?rev=1306
Source3:        FindSWIG.cmake

#Allow the find Cmake find script to find OpenMPI
#Patch0:         %{name}_mpi.patch
Patch1:         %{name}-python-sitedir.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gdal-devel
BuildRequires:  swig
BuildRequires:  sqlite-devel
BuildRequires:  proj-devel
BuildRequires:  expat-devel
BuildRequires:  cmake

BuildRequires:  libcurl-devel
BuildRequires:  gsl-devel
BuildRequires:  libX11-devel
BuildRequires:  cxxtest

# We don't want to provide private python extension libs
%{?filter_setup:
%filter_provides_in %{python_sitearch}/.*\.so$ 
%filter_setup
}

%description
OpenModeller is an open source C++ library to determine potential 
distribution in environment and geographic space. Models are generated 
by algorithms that receive as input a set of occurrence points 
(latitude/longitude/abundance) and a set of raster files with environment
data. Models can then be projected to generate distribution maps. 
Available algorithms include Bioclim, GARP Best Subsets, Support Vector 
Machines, Climate Space Model, Environmental Distance, AquaMaps and
Maximum Entropy. The library uses GDAL to read different raster formats 
and proj4 to convert between different coordinate systems and projections. 


%package devel
Summary:        Development Libraries for %{name} 
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
Development package for %{name} including header files.

%package python
Summary:        Python bindings for %{name} 
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description python
Python bindings for %{name}.

#Im test-Verzeichnis sind kompatible Lizenzen, womöglich aber ein Bundle und falsche FSF-Adressen

%prep
%setup -q -n lib%{name}-src-%{version}

#SOAP?
#%patch0 -p1 -b .mpi
%patch1 -p1 -b .python-sitedir

install -pm 644 %{SOURCE1} cmake
install -pm 644 %{SOURCE2} cmake
install -pm 644 %{SOURCE3} cmake

# Delete invalid first lines in man pages
pushd src/console/man
  for file in *; do
    sed -i '1d' $file
  done
popd


#Correct path for 64 bit libraries
#Da sollte man eher prefix schreiben
if [ "%{_libdir}" = "/usr/lib64" ] ; then
    sed -i 's/\lib/\/lib64/g' CMakeLists.txt
    sed -i 's/LIBRARY DESTINATION lib/LIBRARY DESTINATION lib64/g' src/%{name}/CMakeLists.txt
fi
#Correct installation directory for man pages
sed -i 's/\(man\/man1\)/share\/\1/g' src/console/CMakeLists.txt
#-Werror cancels compilation
#sed -i '/ADD_DEFINITIONS( -Wall -Werror )/d' CMakeLists.txt
#OM_CTEST

#Correct permissions of source files
chmod 644 src/%{name}/*.cpp src/algorithms/enfa/*.cpp src/algorithms/enfa/*.hh

#Change encoding to UTF-8
iconv -f iso8859-1 -t utf-8 AUTHORS > AUTHORS.conv && mv AUTHORS.conv AUTHORS
pushd src/console/man
    iconv -f iso8859-1 -t utf-8 om_niche.1 > om_niche.1.conv && mv om_niche.1.conv om_niche.1
    iconv -f iso8859-1 -t utf-8 om_console.1 > om_console.1.conv && mv om_console.1.conv om_console.1
popd
%build
# Upstream turns off pedantic as well in their RPM
# MPI support is not ready upstream
%cmake -DCMAKE_INSTALL_PREFIX:PATH=%{_prefix}   \
    -DCMAKE_SKIP_RPATH:BOOL=ON                  \
    -DPEDANTIC:BOOL=OFF                         \
    -DOM_BUILD_SERVICE:BOOL=ON                  \
    -DOM_CTEST:BOOL=ON                          \
    -DOM_BUILD_BINDINGS:BOOL=ON                 \
    -DGDAL_LIBRARY:PATH=%{_libdir}/libgdal.so

make %{?_smp_mflags}
# Pluginpfad ist schlecht
#Doxygen
    #-DLIBRARY_OUTPUT_PATH=%{_libdir}/%{name} \ -- Wird nicht perfekt verwendet
    #-DOM_CTEST:BOOL=ON \
#Bei Algorithms könnte ein Problem sein
#Installationspfad
#Beispieldaten
%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%check
#TEST

%clean
rm -rf %{buildroot}

#pkg-config?
%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING.txt ChangeLog README.txt 
%config(noreplace) %{_sysconfdir}/%{name}
%{_bindir}/*
%{_libdir}/%{name}
%{_libdir}/lib%{name}.so
%{_mandir}/man1/*
%{_datadir}/%{name}

%files devel
%defattr(-,root,root,-)
%{_includedir}/%{name}

%files python
%defattr(-,root,root,-)
%{python_sitearch}/om.py*
%{python_sitearch}/_om.so

%changelog
* Sun Jan 23 2011 Volker Fröhlich <volker27@gmx.at> 1.1.0-1
- Initial packaging for Fedora
