%global shortname spatialindex
Name:      libspatialindex
Version:   1.6.1
Release:   1%{?dist}
Summary:   Spatial index library 
Group:     System Environment/Libraries
License:   LGPL
URL:       http://trac.gispython.org/spatialindex
Source0:   http://download.osgeo.org/libspatialindex/%{shortname}-src-%{version}.tar.bz2

BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildRequires:  chrpath

%description
Spatialindex provides a general framework for developing spatial indices.
Currently it defines generic interfaces, provides simple main memory and
disk based storage managers and a robust implementation of an R*-tree,
an MVR-tree and a TPR-tree.

%package devel
Summary: Development Libraries for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Development files for %{name}.

%prep
%setup -qn %{shortname}-src-%{version}

%build

%configure
#        --enable-doxygen-man=NO

##  WARNING
##  disable %{?_smp_mflags}
##  it breaks compile

# Can't prove that true.
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

chrpath --delete %{buildroot}%{_libdir}/%{name}_c.so.1.0.0

# Delete libtool archives, because we don't ship them.
pushd %{buildroot}%{_libdir} 
 rm -f *.a *.la
popd

#Tests?

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files 
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING README
%{_libdir}/%{name}*.so
%{_libdir}/%{name}*.so.1

%files devel
%defattr(-,root,root,-)
%{_includedir}/%{shortname}
%{_libdir}/%{name}*.so.1.*

%changelog
* Sun Feb 06 2011 Volker Fröhlich <volker27@gmx.at> - 1.6.1-1
- Initial packaging
