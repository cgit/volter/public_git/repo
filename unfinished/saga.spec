Name:           saga
Version:        2.0.6        
Release:        1%{?dist}
Summary:        Geographic information system with an API for processing geodata 

Group:          Applications/Engineering
# Überall steht V2, bei manchen Modulen aber auch V2 or later; V3 steht nirgends, Adresse falsch; Teile sind absichtlich LPGL (Zumindest AUCH 2.1)
License:        GPLv3 
URL:            http://www.saga-gis.org 
Source0:        http://downloads.sourceforge.net/%{name}/%{name}_%{version}.tar.gz
#/download 

BuildRequires: wxGTK-devel 
BuildRequires: proj-devel 
BuildRequires: gdal-devel 
BuildRequires: jasper-devel 
BuildRequires: libtiff-devel 
BuildRequires: unixODBC-devel 
BuildRequires: libharu-devel
BuildRequires: vigra-devel
# Das gehört letztlich zu libpng bzw. jasper
BuildRequires: g2clib-devel
BuildRequires: libtool automake autoconf

%description
SAGA is a geographic information system (GIS), with a special
'Application Programming Interface' (API) for geographic data processing.
This API makes it easy to implement new algorithms. The SAGA API supports
grid data, vector data, and tables. 

%prep
%setup -q
chmod -x src/modules_io/grid/io_grid_grib2/g2clib-1.0.4/*

sed -i 's/\r//' AUTHORS

%build
autoreconf -i
%configure --enable-unicode --disable-static
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# Drop libtool archives
rm -f %{buildroot}/%{_libdir}/saga/lib*.la %{buildroot}/%{_libdir}/saga/libsaga_*.la
# TODO: Unused dependencies
# TODO: Desktop-File und so
# Ist da mit Python was?
# Beispieldaten-Tarball gäbe es
# G2CLIB ist dabei! Die haben wir eh. Lizenz OK?
# Esri-Schmarren ist sauber?

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc COPYING AUTHORS ChangeLog
%{_libdir}/saga
%{_bindir}/saga*
%{_libdir}/libsaga_*
%{_mandir}/man1/saga_*

%changelog
* Fri Nov 26 2010 Volker Fröhlich <volker27@gmx.at> - 2.0.6-1
- Initial packaging
