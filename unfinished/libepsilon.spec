# Define a macro for calling ../configure instead of ./configure
%global dconfigure %(printf %%s '%configure' | sed 's!\./configure!../configure!g')

%global short_name epsilon

Name:      libepsilon
Version:   0.8.1
Release:   3%{?dist}
Summary:   Powerful wavelet image compressor
Group:     System Environment/Libraries
License:   GPLv2
URL:       http://sourceforge.net/projects/%{short_name}-project
Source0:   http://downloads.sourceforge.net/%{short_name}-project/%{short_name}-%{version}.tar.gz
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

Requires:  %{name}-common = %{version}-%{release}
BuildRequires: popt-devel
#BuildRequires: glibc-devel.i686

%description
EPSILON is a powerful OpenSource wavelet image compressor.
Wavelet-driven compressors are know to be much more effective
than traditional DCT-based ones (like JPEG). At the moment,
the program supports 30+ different wavelet filters, runs
in parallel in multi-threaded and MPI environments, can
process HUGE images and much more.

%package common
Summary:  Common Powerful wavelet image compressor
%description common
EPSILON is a powerful OpenSource wavelet image compressor.

%package openmpi
Summary:   MPI-Powerful wavelet image compressor
BuildRequires: openmpi-devel
# Require explicitly for dir ownership and to guarantee the pickup of the right runtime
Requires: openmpi
Requires: %{name}-common = %{version}-%{release}
%description openmpi
EPSILON is a powerful OpenSource wavelet image compressor.

%package mpich2
Summary:   MPICH2-Powerful wavelet image compressor
BuildRequires: mpich2-devel
# Require explicitly for dir ownership and to guarantee the pickup of the right runtime
Requires: mpich2
Requires: %{name}-common = %{version}-%{release}
%description mpich2
EPSILON is a powerful OpenSource wavelet image compressor.

%package devel
Summary: Development Libraries for libepsilon
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Contains libraries and header files for
developing applications that use libepsilon.


%prep
%setup -q -n %{short_name}-%{version}
#sed -i -e 's! -shared ! -Wl,--as-needed\0!g' libtool


%build

#MPI scheint zu funktionieren, mpich2 fliegt raus

# To avoid replicated code define a build macro
%define dobuild() \
mkdir $MPI_COMPILER; \
cd $MPI_COMPILER; \
%dconfigure --enable-pthreads --enable-cluster --disable-static --program-suffix=$MPI_SUFFIX ;\
make %{?_smp_mflags} ; \
cd ..

# Build serial version, dummy arguments
MPI_COMPILER=serial MPI_SUFFIX= %dobuild

# Build parallel versions: set compiler variables to MPI wrappers
#export CC=mpicc
export CXX=mpicxx
#export FC=mpif90
export F77=mpif77

# Build OpenMPI version
%{_openmpi_load}
%dobuild
%{_openmpi_unload}

# Build mpich2 version
%{_mpich2_load}
%dobuild
%{_mpich2_unload}


#%configure \
#        CPPFLAGS="-I/usr/include/openmpi-x86_64/" LDFLAGS="-lmpi"\
#        --prefix=%{_prefix} \
#        --enable-pthreads \
#        --enable-cluster \
#        --disable-static \
#        --enable-mpi

%install
rm -rf %{buildroot}

# Install serial version
make -C serial install DESTDIR=%{buildroot} INSTALL="install -p" CPPROG="cp -p"

# Install OpenMPI version
%{_openmpi_load}
make -C $MPI_COMPILER install DESTDIR=%{buildroot} INSTALL="install -p" CPPROG="cp -p"
%{_openmpi_unload}

# Install MPICH2 version
%{_mpich2_load}
make -C $MPI_COMPILER install DESTDIR=%{buildroot} INSTALL="install -p" CPPROG="cp -p"
%{_mpich2_unload}


# Remove libtool archive
rm -f %{buildroot}/%{_libdir}/%{name}.la
rm -f %{buildroot}/%{_libdir}/%{name}.a
# Das sollte eigentlich auch von selbst gehen.

%clean
rm -rf %{buildroot}


# Single-Variante auch anbieten
# Wenn möglich, Versionen für jeden MPI-Compiler in Fedora. openmpi (f, rh), mpich (rh), mpich2 (f) , mvapich und mvapich2(rh), lam (rh)
# Spezifische Dateien müssen in $MPI_BIN, $MPI_LIB und so
# Binaries brauchen ein Suffix
# Keine ldconfig zulässig!
# Gemeinsames Zeug in -common-Paket das alle benötigen
# Eigene Header-Pakete und Devel-Pakete
#TODO: Options, Doxygen?

#Die Perl-Skripts und man-Pages sind genau gleich. Bins scheinen sich zu unterscheiden. Was ist mit der Bib? Die wird nur einmal gemacht.

%files 
%defattr(-,root,root,-)
%{_bindir}/%{short_name}


%files devel
%defattr(-,root,root,-)
%{_libdir}/*.so
%{_includedir}/*

%files common
%defattr(-,root,root,-)
%doc README AUTHORS ChangeLog TODO COPYING
%{_bindir}/start_%{short_name}_nodes.pl
%{_bindir}/stop_%{short_name}_nodes.pl
%{_libdir}/*.so.*
%{_mandir}/man1/*

%files openmpi
%defattr(-,root,root,-)
%{_bindir}/epsilon_openmpi

%files mpich2
%defattr(-,root,root,-)
%{_bindir}/epsilon_mpich2

%changelog
* Wed Dec 8 2010 Volker Fröhlich <volker27@gmx.at> 0.8.1-3
- Added MPI support

* Mon Dec 5 2010 Volker Fröhlich <volker27@gmx.at> 0.8.1-2
- Included license
- Enable cluster and pthreads

* Sun Dec 5 2010 Volker Fröhlich <volker27@gmx.at> 0.8.1-1
- Initial packaging for Fedora
