Name:           libgaiagraphics
Version:        0.4
Release:        2%{?dist}
Summary:        Graphics canvas for GIS rendering

Group:          System Environment/Libraries
#TODO: Stimmt angeblich nicht
License:        GPLv3+
URL:            http://www.gaia-gis.it/spatialite-2.4.0-4
Source0:        http://www.gaia-gis.it/spatialite-2.4.0-4/libgaiagraphics-%{version}.tar.gz

# Fedora's libgeotiff ships with pkgconfig.
# Since that is not standard, informing upstream is not useful.
Patch0:         libgaiagraphics_libgeotiff.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libgeotiff-devel 
BuildRequires:  libjpeg-devel libpng-devel
#Not linked but checked for
BuildRequires:  zlib-devel
BuildRequires:  proj-devel
BuildRequires:  cairo-devel
BuildRequires:  autoconf
Requires:       pkgconfig

%description
Libgaiagraphics wraps raster- and vector graphics, to implement a reasonably
abstract and platform independent graphics canvas for GIS rendering.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q

#TODO: Warning autoconf
%patch0 -p1 -b .libgeotiff

%build
# configure.ac and Makefile.ini are patched, hence the need to run autoconf.
autoconf
%configure --disable-static

# Remove links to unused libraries
sed -i -e 's! -shared ! -Wl,--as-needed\0!g' libtool


make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# Delete libtool archives, because we don't ship them.
find %{buildroot} -name '*.la' -exec rm -f {} ';'


%clean
rm -rf %{buildroot}


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING
%{_libdir}/%{name}.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/gaiagraphics.h
%{_libdir}/%{name}*.so
%{_libdir}/pkgconfig/gaiagraphics.pc

%changelog
* Fri Feb 18 2011 Volker Fröhlich <volker27@gmx.at> - 0.4-2
- Corrected spelling of the name in description
- More specific file list
- Added Requires for pkgconfig (EPEL 5)
- Switched to version macro in source url

* Tue Jan 18 2011 Volker Fröhlich <volker27@gmx.at> - 0.4-1
- Initial packaging
