Name:		pgRouting
Version:	1.05
Release:	1%{?dist}
Summary:	Provides routing functionality to PostGIS/PostgreSQL

Group:		Applications/Databases
# shooting_star* are under the latter license
License:	GPLv2+ and Boost
URL:		http://pgrouting.org/
Source0:    http://download.osgeo.org/pgrouting/source/pgrouting-%{version}.tar.gz
# Removes the preset CFLAGS and CXXFLAGS
Patch2:	    pgrouting-1.05-flags.patch	

BuildRequires:	boost-devel
BuildRequires:	CGAL-devel
BuildRequires:	cmake
BuildRequires:	geos-devel
BuildRequires:	postgresql-devel
BuildRequires:	proj-devel

Requires:	postgis
Requires:	postgresql

%description
The main objective is to provide routing functionality to PostGIS/ PostgreSQL.
pgRouting is part of PostLBS, which provides core tools for Location Based 
Services (LBS) as Open Source Software (OSS). Its tools are similar to those
found on proprietary software.

%prep
%setup -qn pgrouting-%{version}

%patch2 -p1 -b .flags
# fixing boost property_map files path
sed -i "s|boost/vector_property_map.hpp|boost/property_map/vector_property_map.hpp|" core/src/shooting_star_boost_wrapper.cpp 
sed -i "s|boost/property_map.hpp|boost/property_map/property_map.hpp|" core/src/shooting_star_relax.hpp
sed -i "s|boost/property_map.hpp|boost/property_map/property_map.hpp|" core/src/edge_visitors.hpp
# fix to avoid deprecated "boost/graph/detail/is_same.hpp" file
sed -i "s|boost/graph/detail/is_same.hpp|boost/type_traits/is_same.hpp|" core/src/edge_visitors.hpp
# fixing CMakeList issue
cd cmake; ln -s CMakeList.txt CMakeLists.txt; cd -

%build

install -d build
cd build

# TSP requires libgaul http://sourceforge.net/projects/gaul
# Last update there was in 2009
%cmake .. \
	-DWITH_TSP=OFF \
	-DWITH_DD=ON 

make VERBOSE=1 %{?_smp_mflags}

%install
make -C build install DESTDIR=%{buildroot}

%files
%defattr(-,root,root,-)
%doc README.routing COPYING authors.txt BOOST_LICENSE_1_0.txt RELEASE_NOTES
%{_libdir}/pgsql/*.so
%{_datadir}/postlbs

%changelog
* Tue Mar 08 2011 Volker Fröhlich <volker27@gmx.at> - 1.05-1
- Updated to 1.05
- Updated URL
- Retired patch0
- Updated flags patch: optflags work now
- Replaced make macros with the command
- Dropped rm for buildroot before install
- Dropped ldconfig, because libs are in sub-directory
- Dropped version requirements in BRs and Requires.
  All shiped versions are new enough in Fedora
- Enabled DD option and added BR CGAL-devel
- Simplified attr and dropped defattr in files section

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.03-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sun Nov 14 2010 Viji Nair <viji [AT] fedoraproject DOT org> - 1.03-3
- Fixed patch naming conventions.
- Added backup option for files being patched with suffix.
- Changed the package name from postgresql-pgrouting to pgRouting

* Sun Nov 14 2010 Viji Nair <viji [AT] fedoraproject DOT org> - 1.03-2
- Changed the license to GPLv2+ and Boost.
- Removed the override of CMAKE_INSTALL_PREFIX.
- Removed test for %%{?_lib}.
- Added VERBOSE=1 and %%{?_smp_mflags} for make.
- Patch for removing the preset CFLAGS.
- Removed gcc-c++ from BuildRequires.

* Thu Nov 11 2010 Viji Nair <viji [AT] fedoraproject DOT org> - 1.03-1
- Initial import.
