Name:      tinyows
Version:   323svn
Release:   1%{?dist}
Summary:   WFS-T and FE implementation server

Group:     Applications/Publishing
License:   MIT
URL:       http://www.tinyows.org
Source0:   http://tinyows.org/tracdocs/release/%{name}-%{version}.tar.bz2
#Source1:   no_date_footer.html
#Requires:  httpd # Nicht zwangsläufig den
Requires:  postgis >= 1.5

BuildRequires: ctags valgrind indent
BuildRequires: graphviz doxygen 
BuildRequires: libxml2-devel postgresql-devel fcgi-devel

%description
TinyOWS server implements latest WFS-T standard versions,
as well as related standards such as Filter Encoding (FE).

%prep
%setup -q

%build
autoconf
%configure \
    --with-fastcgi \
    --with-pg_config
    #--prefix=%{_prefix}
# Ungültig


# fix datadir lookup path
sed -i -e 's|/usr/tinyows/|%{_datadir}/%{name}/|' src/ows_define.h
sed -i -e 's|/usr/tinyows/|%{_datadir}/%{name}/|' test/config.xml
#Womöglich nöt
# fix DSO lookup
#sed -i -e 's|-lpq|-lpq -lm|' Makefile

# WARNING
# disable %{?_smp_mflags}
# it breaks compile
make %{?_smp_mflags}

# disable timestamp inside docs
#sed -i -e 's|HTML_FOOTER|HTML_FOOTER=no_date_footer.html\n\#|g' doc/Doxyfile
#make doxygen

%install
rm -rf %{buildroot}

make install PREFIX=%{buildroot}%{_datadir}
#make install-test
install -d %{buildroot}%{_bindir}
install -p -m 0755 tinyows %{buildroot}%{_bindir}/
install -d %{buildroot}%{_sysconfdir}/%{name}
install -p -m 0644 test/config.xml %{buildroot}%{_sysconfdir}/%{name}
pushd %{buildroot}%{_datadir}/%{name}/
   ln -s ../../..%{_sysconfdir}/%{name}/config.xml config.xml
popd

%clean
rm -rf %{buildroot}

%check

# Im Moment fliegt man da komplett raus (Mem leak)
#make %{?_smp_mflags} test-valgrind 
#|| true

%files
%defattr(-,root,root,-)
%doc LICENSE README VERSION
#%doc doc/doxygen
%{_bindir}/%{name}
%{_datadir}/%{name}
%dir %{_sysconfdir}/%{name}
%attr(644,root,root) %config(noreplace) %{_sysconfdir}/%{name}/config.xml

%changelog
* Sat Sep 11 2010 Volker Fröhlich <volker27@gmx.at> - 0.9.0-1
- bla 

* Thu Mar 18 2010 Balint Cristian <rezso@rdsor.ro> - 0.7.0-1
- new upstream

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Jan 05 2009 Balint Cristian <rezso@rdsor.ro> - 0.6.0-3
- initial import

* Thu Sep 25 2008 Balint Cristian <rezso@rdsor.ro> - 0.6.0-2
- shorter summary description

* Thu Sep 25 2008 Balint Cristian <rezso@rdsor.ro> - 0.6.0-1
- initial package for fedora
