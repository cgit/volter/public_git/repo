Name:           gpstk
Version:        1.7
Release:        0.1.RC%{?dist}
Summary:        The GPS Toolkit

Group:          Applications/Engineering
License:        LGPLv2+
URL:            http://www.gpstk.org/
Source0:        http://downloads.sourceforge.net/%{name}/%{name}-%{version}-RC.src.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)     

BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  jam
BuildRequires:  libtool
BuildRequires:  doxygen
#BuildRequires:  gtkmm24-devel
#BuildRequires:  libglademm24-devel
#BuildRequires:  gconfmm26-devel
BuildRequires:  zlib-devel
BuildRequires:  libpng-devel
BuildRequires:  libtiff-devel
BuildRequires:  ncurses-devel


%description
Algorithms and frameworks supporting the development of processing
and analysis applications in navigation and global positioning.

%package devel
Summary:         Development files for GPSTk
Group:           Development/Libraries
Requires:        %{name} = %{version}-%{release}

%description devel
This package contains the development files for the GPS Toolkit.


%package docs
Summary:          Documentation for GPSTk
Group:            Documentation

%description docs
This package contains the HTML documentation files for the GPS Toolkit.


%prep
%setup -q -n %{name}-%{version}-RC


%build
./autogen.sh
%configure --prefix=%{_prefix} 

#doxygen
jam -j %{?_smp_mflags} 


%install
rm -rf %{buildroot}
jam -s PREFIX=%{buildroot}%{_prefix} install

# Jam installs everything in /lib, but 64-bit libraries should be in /lib64  
%if "%{_lib}" == "lib64"
mv %{buildroot}%{_prefix}/lib %{buildroot}%{_libdir}
%endif

# Remove libtool archives
rm %{buildroot}/%{_libdir}/*.a

# ldconfig in post is not enough for some reason
pushd %{buildroot}%{_libdir}
    find . -name '*.so.17.0' -print 2>/dev/null -exec chmod 755 {} \;
    ldconfig -n .
popd

# TODO: Tests?


%clean
rm -rf %{buildroot}


%post -p /sbin/ldconfig


%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README
%{_bindir}/*
%{_libdir}/*

%files devel
%defattr(-,root,root,-)
%{_includedir}/%{name}/

%files docs
%defattr(-,root,root,-)
#%doc doc/html


%changelog
* Fri Dec 18 2009 Volker Fröhlich <volker27@gmx.at> - 1.7-0.1.RC
- Updated to 1.7 RC

* Mon Oct 05 2009 Fabian Affolter <fabian@bernewireless.net> - 1.6-1
- Updated to new upstream version 1.6

* Sat Jan 24 2009 Fabian Affolter <fabian@bernewireless.net> - 1.5-1
- Initial package for Fedora
