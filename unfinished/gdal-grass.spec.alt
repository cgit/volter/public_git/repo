%define grasspath %{_libdir}/grass-6.4.0

Name:		gdal-grass
Version:	1.4.3
Release:	1%{?dist}
Summary:	Standalone GRASS 6 Drivers for GDAL and OGR

Group:		Applications/Engineering
License:	MIT
URL:		http://www.gdal.org/
Source0:	http://download.osgeo.org/gdal/%{name}-%{version}.tar.gz
Patch0:		gdal-grass-1.4.3-ldflags.patch
Patch1:		gdal-grass-1.7.3-prefix.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: grass >= 6.3.0
Requires: grass-libs >= 6.3.0
Requires: proj >= 4.5.0
Requires: zlib >= 1.2.3
Requires: gdal >= 1.6.0
BuildRequires: grass >= 6.3
BuildRequires: grass-libs >= 6.3
BuildRequires: grass-devel >= 6.3
BuildRequires: proj-devel >= 4.5.0
BuildRequires: zlib-devel >= 1.2.3
BuildRequires: gdal-devel >= 1.6.0

%description
This package contains standalone drivers for GRASS raster and vector
files that can be built after GDAL has been built and installed as an
"autoload" driver.

This is particularly useful in resolving problems with GRASS depending
on GDAL, but GDAL with GRASS support depending on GRASS.  With this
package you can configure and install GDAL normally (--without-grass), then
build and install GRASS normally and finally build and install this driver.

To build this driver it is necessary for it to find GDAL and GRASS support
files.

%prep
%setup -q -n %{name}-%{version}
%patch0 -p0 -b .ldflags 
%patch1 -p0 -b .prefix

%build
%configure	\
	--enable-shared					\
	--with-grass=%{grasspath}			\
	--with-autoload=%{_libdir}/gdal-grass/plugins	\
	--with-gdal=/usr/bin/gdal-config

make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

# make plugin available on system
install -d %{buildroot}/etc/ld.so.conf.d
install -d %{buildroot}/etc/profile.d
echo "%{_libdir}/gdal-grass/plugins" > %{buildroot}/%{_sysconfdir}/ld.so.conf.d/gdal-grass.conf
echo "export GDAL_DRIVER_PATH=%{_libdir}/gdal-grass/plugins" > %{buildroot}/%{_sysconfdir}/profile.d/gdal-grass.sh

%clean
rm -rf $RPM_BUILD_ROOT

%post 
/sbin/ldconfig
if [ -d %{_libdir}/grass-*/driver/db ] ; then
	cp -a %{_libdir}/grass-*/driver/db %{_libdir}/gdal-grass/driver/
fi
if [ -f %{_libdir}/grass-*/etc/ellipse.table ]; then
	cp -p %{_libdir}/grass-*/etc/ellipse.table %{_libdir}/gdal-grass/etc/
fi
if [ -f %{_libdir}/grass-*/etc/datum.table ]; then
	cp -p %{_libdir}/grass-*/etc/datum.table %{_libdir}/gdal-grass/etc/
fi
if [ -f %{_libdir}/grass-*/etc/datumtransform.table ]; then
	cp -p %{_libdir}/grass-*/etc/datumtransform.table %{_libdir}/gdal-grass/etc/
fi
echo -e "\e[0;34mPlease \e[0;31mlogout and login \e[0;34magain or \
run \e[0;31mexport GDAL_DRIVER_PATH=%{_libdir}/gdal-grass/plugins \
\e[0;34mto set the plugin path. To verify the installation run \
\e[0;31mgdal_translate --formats | grep -i grass. \e[0;34mThis should \
return something similar to \e[0;31mGRASS (ro): GRASS Database Rasters \
(5.7+)\e[0m"

%postun 
rm -rf %{_libdir}/gdal-grass
/sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_libdir}/gdal-grass/plugins/*.so
%{_libdir}/gdal-grass/etc/
%{_libdir}/gdal-grass/driver/
%{_sysconfdir}/ld.so.conf.d/gdal-grass.conf
%{_sysconfdir}/profile.d/gdal-grass.sh
%doc README

%changelog
* Wed Nov 24 2010 Viji Nair <viji [AT] fedoraproject DOT org> - 1.4.3-1
-Initial Build
