%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%endif
# Can be removed after EOL of F12 (at least for Fedora)

Name:           libLAS
Version:        1.6.0
Release:        13%{?dist}
Summary:        Library and tools for the LAS LiDAR format

Group:          System Environment/Libraries
License:        BSD style and Boost
URL:            http://liblas.org/
#
#  The next release will be similar to:
#    http://download.osgeo.org/liblas/libLAS-1.6.0b3.tar.gz
#
#  For now, we use a snapshot generated on:
#    Wed Jan 12 12:49:27 EST 2011
#  using:
#    hg clone http://hg.liblas.org/main libLAS
#    cd libLAS
#    hg archive ../libLAS-2738:e3efe5548e3b.tar.gz
#
#  and the above was done to obtain shared-library versioning.
#
Source0:        libLAS-2738:e3efe5548e3b.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  boost-devel, cmake
BuildRequires:  libxml2-devel, libgeotiff-devel
# Requires:     

%description
libLAS is a C/C++ library for reading and writing the very common LAS LiDAR
format.  The ASPRS LAS format is a sequential binary format used to store data
from LiDAR sensors and by LiDAR processing software for data interchange and
archival storage.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
#
#  TODO : This may be needed for EPEL packages once _libdir/pkgconfig/liblas.pc
#  files are created.
#
#  Requires:       pkgconfig

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%package        python
Summary:        Python files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    python
The %{name}-python package contains python module for using %{name}.

%prep
%setup -q -n 'libLAS-2738:e3efe5548e3b'


%build
#
#  Note: -DWITH_GDAL:BOOL=ON is currently disabled since
#    libLAS requires a not-yet-released GDAL 1.8.x version.
#
export LDFLAGS='-Wl,--as-needed'
%cmake  \
    -DLIBLAS_LIB_SUBDIR:PATH="%{_libdir}"  \
    -DWITH_LIBXML2:BOOL=ON  \
    -DWITH_GEOTIFF:BOOL=ON  \
    -DGEOTIFF_INCLUDE_DIR:PATH="/usr/include/libgeotiff"  \
    -DWITH_GDAL:BOOL=OFF  \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo  \
    -DWITH_PKGCONFIG:BOOL=ON  \
    .
make %{?_smp_mflags}

%{__python} python/setup.py build
%{__python} python/setup.py tests

%check
#
#  The following fails with the message:
#
#    ---> group: liblas::Header, test: test<12>
#         problem: assertion failed
#         failed assertion: `Values are not equal: expected `` actual `+to_meter=1.0000000000``
#    tests summary: failures:1 ok:84
#
#  and this is being investigated so the test is off for now.
#
#make test


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%{__python} setup.py install --skip-build --root %{buildroot}

find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'

#  By default, libLAS installs the files:
#
#    AUTHORS COPYING LICENSE.txt README.txt
#
#  in /usr/share but this is more clearly handled (IMHO) by
#  the doc macro in the files section below.
#
rm -rf $RPM_BUILD_ROOT/usr/share
# chmod 755 $RPM_BUILD_ROOT/usr/bin/*

#  Please see multilib comment below:
#
rm -f $RPM_BUILD_ROOT/usr/bin/liblas-config

%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING LICENSE.txt README.txt
%{_bindir}/las2las
%{_bindir}/las2las-old
%{_bindir}/las2txt
%{_bindir}/las2txt-old
%{_bindir}/lasblock
%{_bindir}/lasinfo
%{_bindir}/lasinfo-old
%{_bindir}/lasmerge
%{_bindir}/ts2las
%{_bindir}/txt2las
%{_libdir}/*.so.*
%{python_sitelib}/liblas
%{python_sitelib}/libLAS.egg-info

%files devel
%defattr(-,root,root,-)
# doc is commented out for now
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/liblas.pc


%changelog
* Wed Jan 12 2011 Ed Hill <ed@eh3.com> - 1.6.0-12
- add the as-needed linker flag

* Wed Jan 12 2011 Ed Hill <ed@eh3.com> - 1.6.0-11
- turn on pkgconfig

* Wed Jan 12 2011 Ed Hill <ed@eh3.com> - 1.6.0-10
- remove macro-in-comment rpmlint warnings 

* Wed Jan 12 2011 Ed Hill <ed@eh3.com> - 1.6.0-9
- new snapshot

* Wed Jan 12 2011 Ed Hill <ed@eh3.com> - 1.6.0-8
- add comments and the check section

* Thu Dec 30 2010 Ed Hill <ed@eh3.com> - 1.6.0-7
- use LIBLAS_LIB_SUBDIR and add TODO concerning multilib

* Wed Dec 29 2010 Ed Hill <ed@eh3.com> - 1.6.0-6
- put liblas-config in the -devel package, fix permissions, and
  add RelWithDebInfo

* Wed Dec 29 2010 Ed Hill <ed@eh3.com> - 1.6.0-5
- use a snapshot with soversion changes and add comments

* Wed Dec 29 2010 Ed Hill <ed@eh3.com> - 1.6.0b3-4
- add SONAME patch

* Wed Dec 29 2010 Ed Hill <ed@eh3.com> - 1.6.0b3-3
- fix license tag

* Wed Dec 29 2010 Ed Hill <ed@eh3.com> - 1.6.0b3-2
- add comment about WITH_GDAL build errors

* Wed Dec 29 2010 Ed Hill <ed@eh3.com> - 1.6.0b3-1
- shared libraries are unversioned so put in main package for now

* Wed Dec 29 2010 Ed Hill <ed@eh3.com> - 1.6.0b3-0
- initial package creation

