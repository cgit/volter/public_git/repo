Name:      qextserialport
Version:   1.2.0
#Stimmt nicht ganz
Release:   1%{?dist}
Summary:   Qt interface class for old fashioned serial ports
Group:     System Environment/Libraries
License:   BSD
URL:       http://code.google.com/p/qextserialport/
Source0:   %{name}-%{version}.tar.gz

# Introduces placeholders in Qmake's .pri and .pro file to be substituted later
#Patch0:    %{name}_path.diff

BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

#BuildRequires: qt4-devel

%description
QextSerialPort provides an interface to old fashioned serial ports for 
Qt-based applications. It currently supports Mac OS X, Windows, Linux, FreeBSD.

%package devel
Summary:        Development Libraries for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
This package contains the files necessary
to develop applications using Qextserialport.

%prep
%setup -qn %{name}

# Encode Changelog to utf-8
iconv -f iso8859-1 -t utf-8 ChangeLog > ChangeLog.conv && mv ChangeLog.conv ChangeLog 

#%patch0 -p1 -b .path

# Set include an lib paths; path for documentation
#sed -i "s\LIBPATH\ %{buildroot}/%{_libdir}\1" qwtpolar.pri
#sed -i "s\HEADERPATH\ %{buildroot}/%{_includedir}/%{name}\1" qwtpolar.pri
#sed -i "s\DOCPATH\ %{buildroot}/%{_docdir}/%{name}-%{version}\1" qwtpolar.pri
#sed -i "s|/path/to/qwt-5.2/include|%{_includedir}/qwt|" qwtpolar.pri
#sed -i "s|/path/to/qwt-5.2/lib|%{_libdir}|" qwtpolar.pri

# Add buildroot to installation path for Designer plug-in
#sed -i "s|BUILDROOT|%{buildroot}|" designer/designer.pro


%build

qmake-qt4
make %{?_smp_mflags}

%install
#rm -rf %{buildroot}

#Das tut gar nix
make install DESTDIR=%{buildroot}

#
#mv %{buildroot}%{_docdir}/%{name}/man/man3/* %{buildroot}/%{_mandir}/man3/%{name}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files 
%defattr(-,root,root,-)
%doc ChangeLog
#%{_libdir}/lib%{name}.so.*

%files devel
%defattr(-,root,root,-)
#%{_includedir}/%{name}

%changelog
* Sun Feb 20 2011 Volker Fröhlich <volker27@gmx.at> 1.2.0-1
- Initial packaging for Fedora
