%global capname osgEarth

#TODO: libosg?

Name:           osgearth
Version:        2.0
Release:        1%{?dist}
Summary:        Terrain rendering toolkit for OpenSceneGraph 

Group:          Applications/Engineering
License:        LGPLv2+
URL:            http://www.osgearth.org/
Source0:        gwaldron-%{name}-%{name}_%{version}_T2011-02-24-0-g53b997c.tar.gz
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)


BuildRequires:  cmake
BuildRequires:  curl-devel
BuildRequires:  expat-devel
BuildRequires:  gdal-devel
BuildRequires:  geos-devel
BuildRequires:  libzip-devel
BuildRequires:  mesa-libGL-devel
BuildRequires:  mesa-libGLU-devel
BuildRequires:  OpenSceneGraph OpenSceneGraph-devel
BuildRequires:  OpenThreads-devel
BuildRequires:  sqlite-devel
BuildRequires:  zlib-devel

# Replaces hard-coded fonts
Requires: liberation-sans-fonts

%description
OsgEarth is a terrain rendering toolkit for OpenSceneGraph. Just create a
simple XML file, point it at your imagery, elevation, and vector data, load
it into your favorite OSG application, and go! osgEarth supports all kinds of
data and comes with lots of examples to help you get up and running quickly 
and easily. 

%package devel
Summary:        Development Libraries for OsgEarth
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
Development package for osgearth including the C header files.

%package examples
Summary:        Functionality tests and sample data for OsgEarth
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description examples
Functionality tests and according sample data.

%prep
%setup -q -n gwaldron-%{name}-dbaa285

#TODO: LICENSE.txt hat auch was
#sed -i 's/\r//' README.txt
iconv -f iso8859-1 -t utf-8 LICENSE.txt > LICENSE.txt.conv && mv LICENSE.txt.conv LICENSE.txt 

# Hier hat Corsepius nix geändert, glaub ich, daher klappt es nicht.
# Replace hard coded Arial fonts
sed -i -e 's!\(fonts/\)\?arial\.ttf!liberation/LiberationSans-Regular.ttf!g' \
    src/applications/osgearth_symbology/osgearth_symbology.cpp \
    src/osgEarthSymbology/Text.cpp \
    src/osgEarthUtil/Graticule.cpp \
    src/osgEarthUtil/Controls.cpp \
    src/osgEarthFeatures/BuildTextOperator.cpp \
    src/osgEarthFeatures/BuildGeometryFilter.cpp \
    tests/feature_labels.earth

sed -i -e 's!arialbd\.ttf!liberation/LiberationSans-Bold.ttf!g' \
    src/applications/osgearth_controls/osgearth_controls.cpp

#http://git.debian.org/?p=pkg-grass/osgearth.git;a=blob_plain;f=debian/rules;hb=af9134ed336850d2773fd30a6e8480ce32b5d5dc
#Daten für die Tests auch dabei? Tests überhaupt? Wenn nicht, kann ich mir die Binarys wahrscheinlich auch gleich schenken; zumindest _tests, _clouds, _simple
%build
#Build_Type? CMake-install-prefix?
%cmake 
make %{?_smp_mflags}
#Tests händisch verschieben?

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}


%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc LICENSE.txt README.txt
%{_libdir}/lib%{capname}*.so.*
%{_libdir}/osgPlugins-*

#Alle bins in examples
#Warning: font file "liberation/LiberationSans-Regular.ttf" not found. (ocean)

#data und tests in eigenes Paket namens data
#Andere Variante ist ein libosgearth zusätzlich
# Kriegt er dann mit, wenn das noarch ist?

%files devel
%defattr(-,root,root,-)
%{_includedir}/%{capname}*
%{_libdir}/lib%{capname}*.so

%files examples
%defattr(-,root,root,-)
%{_bindir}/%{name}*
#data und tests auch installieren!


%changelog
* Tue Mar 01 2011 Volker Fröhlich <volker27@gmx.at> - 2.0-1
- Initial package
