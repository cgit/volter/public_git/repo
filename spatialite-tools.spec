Name:           spatialite-tools
Version:        2.4.0
Release:        0.3.RC4%{?dist}
Summary:        A set of useful CLI tools for SpatiaLite

Group:          Development/Tools
License:        GPLv3+
Source0:        http://www.gaia-gis.it/spatialite-%{version}-4/%{name}-%{version}.tar.gz
URL:            http://www.gaia-gis.it/spatialite/index.html
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildRequires:  expat-devel
BuildRequires:  libspatialite-devel sqlite-devel proj-devel geos-devel

# No libspatialite for ppc64 -- see #663938
ExcludeArch:    ppc64

%description
A set of useful CLI tools for SpatiaLite.


%prep
%setup -q

# Remove unused Makefiles
rm -f Makefile-static*


%build
# Add libm (floor); Reported upstream via e-mail 15 Jan 2011
%configure LDFLAGS="-lm"
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING
%{_bindir}/*


%changelog
* Fri Feb 25 2011 Volker Fröhlich <volker27@gmx.at> - 2.4.0-0.3.RC4
- Exclude ppc64

* Fri Jan 14 2011 Volker Fröhlich <volker27@gmx.at> - 2.4.0-0.2.RC4
- Dropped prefix from configure macro
- Corrected license
- Use macros in source URL

* Mon Dec 20 2010 Volker Fröhlich <volker27@gmx.at> - 2.4.0-0.1.RC4
- Inital packaging
