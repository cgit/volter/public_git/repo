Name:           spatialite-gui
Version:        1.4.0
Release:        1%{?dist}
Summary:        GUI to manage Spatialite databases

Group:          Applications/Databases
License:        GPLv3
URL:            http://www.gaia-gis.it/spatialite-2.4.0-4
Source0:        http://www.gaia-gis.it/spatialite-2.4.0-4/spatialite_gui-1.4.0.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libspatialite-devel
#BuildRequires:  libgaigraphics-devel kommt mir halt vor, obwohl ich jetzt keinen Beleg sehe
BuildRequires:  wxGTK-devel
BuildRequires:  sqlite-devel
BuildRequires:  geos-devel
BuildRequires:  proj-devel

#Baut bisher nur in Rawhide erfolgreich (getestet mit Mock); dürfte an der Sqlite-Version liegen.

%description
GUI to manage Spatialite databases.

%prep
%setup -q -n spatialite_gui-%{version}
rm -f Makefile-static*
sed -i 's/LIBS = @LIBS@/LIBS = -lproj -lgeos_c @LIBS@/' Makefile.in
#Pfuscherei

%build

%configure LDFLAGS=-lproj \
        --disable-static
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING
%{_bindir}/*


%changelog
* Wed Dec 8 2010 Volker Fröhlich <volker27@gmx.at> 1.4.0-1
- Initial packaging 
