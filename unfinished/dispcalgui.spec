%global capname dispcalGUI

%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%endif

Name:           dispcalgui
Version:        0.6.6.7
Release:        1%{?dist}
Summary:        Graphical frontend for Argyll CMS

Group:          Applications/Desktop
License:        GPLv3
URL:            http://dispcalgui.hoech.net/
#Source falsch
Source0:        http://dispcalgui.hoech.net/%{capname}-%{version}.tar.gz
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

#BuildArch:      noarch
BuildRequires:  python

%description
DispcalGUI is a graphical frontend for several utilities from the open source
color management system Argyll CMS by Graeme W. Gill, specifically dispcal,
dispread, colprof and dispwin, which when used together allow you to calibrate
and profile your display using a measurement device

%package devel
Summary:        Development Libraries for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
Development package for %{name}

%prep
%setup -q -n %{capname}-%{version}


%build
%{__python} setup.py build


%install
%{__python} setup.py install --root %{buildroot}
pushd %{buildroot}
rm -rf home
popd


%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc LICENSE.txt README.html
%{_bindir}/*
%{_mandir}/man1/*
%{_datadir}/%{capname}
%{_datadir}/icons/*
%{_defaultdocdir}/%{capname}-%{version}
%{python_sitearch}/*
%{_sysconfdir}/*
%{_desktopdir}/*

%files devel
%defattr(-,root,root,-)

%changelog

* Thu Dec 23 2010 Volker Fröhlich <volker27@gmx.at> - 0.6.6.7-1
- Initial packaging 
