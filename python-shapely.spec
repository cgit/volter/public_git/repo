%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%endif
# Can be removed after EOL of F12

Name:           python-shapely
Version:        1.2.8
Release:        1%{?dist}
Summary:        Geometric objects, predicates, and operations

Group:          Development/Libraries
License:        BSD
URL:            http://trac.gispython.org/lab/wiki/Shapely
Source0:        http://gispython.org/dist/Shapely-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python-devel
BuildRequires:  python-setuptools-devel

#Tests use numpy, but tests are broken for the time being
#BuildRequires:  numpy

Requires:       geos

%description
Shapely is a package for creation, manipulation, and analysis
of planar geometry objects – designed especially for developers
of cutting edge geographic information systems. In a nutshell:
Shapely lets you do PostGIS-ish stuff outside the context of a
database using idiomatic Python.


%prep
%setup -q -n Shapely-%{version}


%build
%{__python} setup.py build


%check
# Tests fail
# Sean Gillies, one of the authors, said, it was safe to ship either way
# http://trac.gispython.org/lab/ticket/244
#%{__python} setup.py test


%install
rm -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc CHANGES.txt CREDITS.txt README.txt LICENSE.txt
%doc PKG-INFO examples docs
%{python_sitelib}/shapely
%{python_sitelib}/Shapely-%{version}-py*.egg-info
# These two scripts are also in the examples directory, have no shebang
# and seem to serve no purpose otherwise.
%exclude %{_bindir}/intersect.py
%exclude %{_bindir}/dissolve.py


%changelog
* Sat Feb 25 2011 Volker Fröhlich <volker27@gmx.at> - 1.2.8-1 
- Updated for 1.2.8
- Disable tests

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.7-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sat Nov 27 2010 Volker Fröhlich <volker27@gmx.at> - 1.2.7-2 
- Explained excluded files; added check section

* Wed Nov 24 2010 Volker Fröhlich <volker27@gmx.at> - 1.2.7-1 
- Initial package for Fedora
