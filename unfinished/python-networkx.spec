%define short_name networkx

%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%endif
# Can be removed after EOL of F12

Name:           python-networkx
Version:        1.4
Release:        1%{?dist}
Summary:        Create, manipulate and study complex networks

Group:          Development/Libraries
License:        BSD
URL:            http://networkx.lanl.gov/index.html
Source0:        http://pypi.python.org/packages/source/%{short_name}/%{short_name}-%{version}.tar.gz
#Source0:        http://networkx.lanl.gov/download/%{short_name}/%{short_name}-%{version}rc1.tar.gz

#BuildArch:      noarch
BuildRequires:  python-devel python-setuptools-devel

#Es geht auch was mit numpy, scipy, matplotlib, graphviz, pyparsing, pyyaml

%description
NetworkX is a Python package for the creation, manipulation,
and study of the structure, dynamics, and functions of complex networks.

%prep
%setup -q -n %{short_name}-%{version}


%build
%{__python} setup.py build


%check
#%{__python} setup.py test


%install
%{__python} setup.py install --skip-build --root %{buildroot}
#Installiert unerwünschterweise die Dokumentation selbst

rm -rf %{buildroot}/%{_datadir}

%clean


%files
%defattr(-,root,root,-)
%doc README.txt LICENSE.txt
%doc PKG-INFO examples doc
%{python_sitelib}/%{short_name}
%{python_sitelib}/%{short_name}-%{version}-py*.egg-info
# These two scripts are also in the examples directory, have no shebang
# and seem to serve no purpose otherwise.


%changelog
* Mon Jan 17 2011 Volker Fröhlich <volker27@gmx.at> - 1.4-1
- Initial packaging
