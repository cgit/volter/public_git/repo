%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%endif

Name:           qgis
Version:        1.6.0
Release:        7%{?dist}
Summary:        A user friendly Open Source Geographic Information System

Group:          Applications/Engineering
License:        GPLv2+ with exceptions
URL:            http://www.qgis.org/
Source0:        http://qgis.org/~timlinux/src/%{name}_%{version}.tar.gz
Source1:        %{name}.desktop

BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

#TODO
# Fix detection problem for GRASS libraries
# Proposed changes for next version: https://trac.osgeo.org/qgis/ticket/2863
Patch0: qgis-1.5.0-grass.patch
# Fix problem with SIP 4.12, see https://trac.osgeo.org/qgis/changeset/14988
Patch1: qgis-1.6.0-sip.patch

BuildRequires:  cmake
BuildRequires:  flex bison

BuildRequires:  expat-devel
BuildRequires:  gdal-devel
BuildRequires:  geos-devel
BuildRequires:  gsl-devel
BuildRequires:  postgresql-devel
BuildRequires:  proj-devel
BuildRequires:  sqlite-devel
BuildRequires:  libspatialite-devel
BuildRequires:  chrpath
BuildRequires:  desktop-file-utils
BuildRequires:  qt4-devel
BuildRequires:  qwt-devel

#Note for future EPEL version:
#grass is not in EPEL, qt4-webkit does not exist 

BuildRequires:  PyQwt-devel
BuildRequires:  sip-devel > 4.7
BuildRequires:  python-devel
BuildRequires:  PyQt4-devel
BuildRequires:  qt4-webkit-devel
BuildRequires:  fcgi-devel

# Grass package contains definitions needed for configure script
BuildRequires:  grass, grass-devel

Requires:       gpsbabel 

# Obsoletes can be dropped after F15's EOL
Obsoletes:      qgis-theme-classic < 1.1
Obsoletes:      qgis-theme-gis < 1.1
Obsoletes:      qgis-theme-nkids < 1.1

# We don't want to provide private python extension libs
%{?filter_setup:
%filter_provides_in %{python_sitearch}/.*\.so$ 
%filter_setup
}

%description
Quantum GIS (QGIS) is a user friendly Open Source Geographic Information 
System (GIS) that runs on Linux, Unix, Mac OSX, and Windows.
QGIS supports vector, raster, and database formats. QGIS lets you browse
and create map data on your computer. It supports many common spatial data
formats (e.g. ESRI ShapeFile, geotiff). QGIS supports plugins to do things
like display tracks from your GPS.

%package devel
Summary:        Development Libraries for the Quantum GIS
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
Development packages for Quantum GIS including the C header files.

%package grass 
Summary:        GRASS Support Libraries for Quantum GIS
Group:          Applications/Engineering
Requires:       %{name} = %{version}-%{release}
Requires:       grass

%description grass
GRASS plugin for Quantum GIS required to interface with the GRASS system.

%package python 
Summary:        Python integration and plugins for Quantum GIS
Group:          Applications/Engineering
Requires:       %{name} = %{version}-%{release}
Requires:       gdal-python
Requires:       PyQt4
%{?_sip_api:Requires: sip-api(%{_sip_api_major}) >= %{_sip_api}}

%description python
Python integration and plugins for QGIS.

%prep
%setup -q

%patch0 -p1 -b .grass
%patch1 -p1 -b .sip

# Encode man-file to utf-8 -- Solved in 1.7.
iconv -f iso8859-1 -t utf-8 qgis_help.1 > qgis_help.1.conv && mv qgis_help.1.conv qgis_help.1

# Correct version string in title bar
sed -i 's|Trunk|Copiapo|' CMakeLists.txt

# Remove obsolete translations -- They will be there again in 1.7!
#/usr/bin/lupdate-qt4 -noobsolete -ts i18n/qgis_*.ts

%build

#GDAL script doesn't work properly

# Detection doesn't work on it's own
GRASS_PREFIX=%{_libdir}

%cmake \
      %{_cmake_skip_rpath} \
      -D QGIS_LIB_SUBDIR=%{_lib} \
      -D QGIS_MANUAL_SUBDIR=/share/man \
      -D QGIS_PLUGIN_SUBDIR=%{_lib}/%{name} \
      -D QGIS_CGIBIN_SUBDIR=%{_libexecdir}/%{name} \
      -D WITH_BINDINGS:BOOL=TRUE \
      -D BINDINGS_GLOBAL_INSTALL:BOOL=TRUE \
      -D GRASS_PREFIX=$GRASS_PREFIX \
      -D GDAL_INCLUDE_DIR=%{_includedir}/gdal \
      -D GDAL_LIBRARY=%{_libdir}/libgdal.so \
      .
# QGIS Mapserver will be off by default in 1.7

# Parallel make leads to race conditions with PYQT4_WRAP_UI
# https://trac.osgeo.org/qgis/ticket/2880
#make %{?_smp_mflags}
make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%find_lang %{name} --with-qt


# Add executable perms to python libs so they get stripped
chmod +x %{buildroot}%{python_sitearch}/%{name}/*.so

# Remove files packaged by doc or undesired
rm -f %{buildroot}%{_datadir}/%{name}/doc/BUGS \
    %{buildroot}%{_datadir}/%{name}/doc/ChangeLog \
    %{buildroot}%{_datadir}/%{name}/doc/CODING \
    %{buildroot}%{_datadir}/%{name}/doc/COPYING \
    %{buildroot}%{_datadir}/%{name}/doc/INSTALL \
    %{buildroot}%{_datadir}/%{name}/doc/PROVENANCE \
    %{buildroot}%{_datadir}/%{name}/doc/README

# Install desktop file
install -pd %{buildroot}%{_datadir}/pixmaps
install -pm0644 \
    %{buildroot}%{_datadir}/%{name}/images/icons/%{name}-icon.png \
    %{buildroot}%{_datadir}/pixmaps/%{name}.png
desktop-file-install --vendor="fedora" \
--dir=%{buildroot}/%{_datadir}/applications \
%{SOURCE1}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post grass -p /sbin/ldconfig

%postun grass -p /sbin/ldconfig

%post python -p /sbin/ldconfig

%postun python -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc BUGS COPYING ChangeLog README CODING PROVENANCE Exception_to_GPL_for_Qt.txt
# Can be viewed in the About-Box:
# CONTRIBUTORS AUTHORS
%{_libdir}/lib%{name}_*.so.*
%{_libdir}/%{name}/
%{_bindir}/qgis*
%{_mandir}/man1/%{name}*
%{_libexecdir}/%{name}
%dir %{_datadir}/%{name}/
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/applications/fedora-%{name}.desktop
%dir %{_datadir}/%{name}/i18n/
%{_datadir}/%{name}/doc
%{_datadir}/%{name}/images
%{_datadir}/%{name}/resources
%{_datadir}/%{name}/svg
%{_datadir}/%{name}/themes
%exclude %{_libdir}/%{name}/*grass*
%exclude %{_datadir}/%{name}/themes/default/grass
%exclude %{_datadir}/%{name}/themes/gis/grass
%exclude %{_datadir}/%{name}/themes/classic/grass

%files devel
%defattr(-,root,root,-)
%{_includedir}/%{name}
%{_libdir}/lib%{name}_*.so
%{_libdir}/lib%{name}python.so
%{_libdir}/lib%{name}grass.so

%files grass
%defattr(-,root,root,-)
%{_libdir}/lib%{name}grass.so.%{version}
%{_libdir}/%{name}/libgrass*.so
%{_libdir}/%{name}/grass
%{_datadir}/%{name}/grass
%{_datadir}/%{name}/themes/default/grass
%{_datadir}/%{name}/themes/gis/grass
%{_datadir}/%{name}/themes/classic/grass

%files python
%defattr(-,root,root,-)
%{_libdir}/lib%{name}python.so.*
%{_datadir}/%{name}/python
%{python_sitearch}/%{name}

%changelog
* Sat Feb 25 2011 Volker Fröhlich <volker27@gmx.at> - 1.6.0-7
- Added buildroot
- Corrected window title
- Removed encoding key from desktop file and corrected categories

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.6.0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Wed Feb 02 2011 Volker Fröhlich <volker27@gmx.at> - 1.6.0-5
- Remove libspatialite BR
- Forgot to turn off smp-flags

* Wed Feb 02 2011 Volker Fröhlich <volker27@gmx.at> - 1.6.0-4
- Patch for SIP 4.12
- Changed path for QGIS mapserver
- Simplified files section

* Wed Nov 24 2010 Volker Fröhlich <volker27@gmx.at> - 1.6.0-3
- Rebuild for GRASS 6.4

* Thu Nov 11 2010 Volker Fröhlich <volker27@gmx.at> - 1.6.0-2
- Disabled smp-flags again 

* Thu Nov 11 2010 Volker Fröhlich <volker27@gmx.at> - 1.6.0-1
- Avoid rpaths in the first place
- Dropped superfluid QWT and SIP patches
- Dropped test section
- Added dependency for fcgi-devel
- Abbreviated syntax for setup macro
- Qt translations are no longer shipped

* Wed Sep 29 2010 jkeating - 1.5.0-6
- Rebuilt for gcc bug 634757

* Mon Sep 13 2010 Volker Fröhlich <volker27@gmx.at> - 1.5.05
- Added workaround patch for SIP 4.11, see http://trac.osgeo.org/qgis/ticket/2985

* Thu Sep 09 2010 Rex Dieter <rdieter@fedoraproject.org> 1.5.0-4
- rebuild (sip)
- BR: qt4-devel

* Fri Jul 30 2010 Volker Fröhlich <volker27@gmx.at> - 1.5.03
- Added dependency for gdal-python to fulfill standard plugins' requirements

* Tue Jul 27 2010 Rex Dieter <rdieter@fedoraproject.org> - 1.5.0-2.py27
- one more time for python27, with feeling

* Thu Jul 22 2010 David Malcolm <dmalcolm@redhat.com> - 1.5.0-2
- Rebuilt for https://fedoraproject.org/wiki/Features/Python_2.7/MassRebuild

* Sat Jul 17 2010 Volker Fröhlich <volker27@gmx.at> - 1.5.0-1
- Updated for 1.5.0
- Added support for qwt

* Wed Jul 14 2010 Volker Fröhlich <volker27@gmx.at> - 1.4.0-9
- Re-added missing dependency of PyQt4 and sip for python sub-package

* Fri Jul 09 2010 Volker Fröhlich <volker27@gmx.at> - 1.4.0-8
- Further completed qt47 patch

* Fri Jul 09 2010 Volker Fröhlich <volker27@gmx.at> - 1.4.0-7
- Further completed qt47 patch

* Fri Jul 09 2010 Volker Fröhlich <volker27@gmx.at> - 1.4.0-6
- Added patch to remove redundant ::QVariant in function-style cast

* Fri Jul 09 2010 Volker Fröhlich <volker27@gmx.at> - 1.4.0-5
- Removed smp_mflags because of race conditions
- Simplified member initializer syntax in qt47 patch

* Wed Jul 07 2010 Volker Fröhlich <volker27@gmx.at> - 1.4.0-4
- Added preliminary patch for Qt 4.7 -- still won't build for Rawhide because of https://bugzilla.redhat.com/show_bug.cgi?id=587707
- Put version variable in filelist for GRASS
- Added qt-webkit as build require, removed gettext
- Corrected erroneous dependency on GRASS subpackage

* Thu Jul 01 2010 Volker Fröhlich <volker27@gmx.at> - 1.4.0-3
- Changed license to reflect exception for Qt; packaged exception file
- Added find_lang and excluded qt-translations
- Added ownership for directory in share
- Dropped docdir statement
- Changed description for the subpackages python and grass

* Wed Jun 24 2010 Volker Fröhlich <volker27@gmx.at> - 1.4.0-2
- Grouped corresponding entries for description and package macro
- Corrected swapped summaries for python- and grass-subpackage
- Set theme subpackages as obsolete 
- Removed nonsensical build-requires statements from subpackages
- Dropped redundant definition of GRASS_PREFIX
- Removed verbose-option from make
- Removed unnecessary chmod-command
- Removed nonsensical ldconfig for devel-subpackage
- Made the file list more elegant
- Removed unnecessary call for chrpath

* Thu Jun 17 2010 Volker Fröhlich <volker27@gmx.at> - 1.4.0-1
- Updated for 1.4.0
- Dropped theme packages

* Wed Feb 10 2010 Kevin Kofler <Kevin@tigcc.ticalc.org> - 1.0.2-6
- rebuild for sip 4.10
- fix sip version check so it works with sip 4.10 (#553713)

* Wed Jan 13 2010 Devrim GÜNDÜZ <devrim@gunduz.org> - 1.0.2-5
- Rebuild against new GEOS.

* Wed Dec 23 2009 Rex Dieter <rdieter@fedoraproject.org> - 1.0.2-4
- qgis rebuild for sip-4.9.x (#538119)

* Fri Dec 04 2009 Devrim GÜNDÜZ <devrim@gunduz.org> - 1.0.2-3
- Rebuild for new Geos.

* Tue Nov 17 2009 Rex Dieter <rdieter@fedoraproject.org> - 1.0.2-2 
- -python: Requires: sip-api(%%_sip_api_major) >= %%_sip_api (#538119) 

* Thu Oct 22 2009 Alex Lancaster <alexlan[AT] fedoraproject org> - 1.0.2-1.1
- Rebuilt to fix python problem (#518121)

* Thu Jul 30 2009 Douglas E. Warner <silfreed@silfreed.net> 1.0.2-1
- updating for 1.0.2
- moving libqgispython.so to python subpackage for bug#507381

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Feb 18 2009 Douglas E. Warner <silfreed@silfreed.net> 1.0.1-1
- updating for 1.0.1

* Mon Dec 22 2008 Douglas E. Warner <silfreed@silfreed.net> 1.0.0-1
- updating for 1.0.0
- added gis theme and quick print plugin
- added requirement for gpsbabel

* Mon Dec 22 2008 Douglas E. Warner <silfreed@silfreed.net> - 0.11.0-8
- cleaning up patch

* Mon Dec 22 2008 Douglas E. Warner <silfreed@silfreed.net> - 0.11.0-7
- bump to add patch

* Thu Dec 18 2008 Douglas E. Warner <silfreed@silfreed.net> - 0.11.0-6
- adding patch to fix typedef problems in python build

* Thu Dec 18 2008 Douglas E. Warner <silfreed@silfreed.net> - 0.11.0-5
- Rebuild for Python 2.6

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.11.0-4
- Rebuild for Python 2.6

* Sun Oct 19 2008 Alex Lancaster <alexlan[AT]fedoraproject org> - 0.11.0-3
- Rebuild for new geos, fixes broken deps

* Mon Aug 11 2008 Douglas E. Warner <silfreed@silfreed.net> 0.11.0-2
- building against blas and lapack instead of atlas and blas to fix missing
  library calls

* Wed Jul 16 2008 Douglas E. Warner <silfreed@silfreed.net> 0.11.0-1
- update to metis 0.11.0
- remove python patch
- enabling python subpackage
- fixed executable perms on new headers/source
- stripping rpath with chrpath
- making python libs executable to get files stripped

* Fri Mar 28 2008 Douglas E. Warner <silfreed@silfreed.net> 0.10.0-2
- added patch to support cmake 2.6

* Fri Mar 28 2008 Balint Cristian <rezso@rdsor.ro> - 0.10.0-1
- upgraded to 0.10.0 release candidate
- removed gcc 4.3 patches
- adding devel package since libraries are now versioned

* Fri Mar 28 2008 Balint Cristian <rezso@rdsor.ro> - 0.9.1-5
- fix build by adding more gcc43 patches
- rebuild against grass63

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.9.1-4
- Autorebuild for GCC 4.3

* Mon Feb 18 2008 Douglas E. Warner <silfreed@silfreed.net> 0.9.1-4
- adding gcc43-INT_MAX-qgis-0.9.1.patch
- adding gcc43-duplicate_function_param-qgis-0.9.1.patch
- adding gcc43-memcpy-qgis-0.9.1.patch
- adding gcc43-memcpy-strtod-qgis-0.9.1.patch

* Mon Feb 18 2008 Douglas E. Warner <silfreed@silfreed.net> 0.9.1-3
- adding Requires sip, PyQt4 for python bindings support

* Mon Jan 28 2008 Douglas E. Warner <silfreed@silfreed.net> 0.9.1-2
- defining lib path in build
- installing python bindings globally
- adding patch to determine python site packages dir correctly

* Mon Dec 17 2007 Douglas E. Warner <silfreed@silfreed.net> 0.9.1-1
- update to 0.9.1
- removing lib64 and man instal path patches (included upstream)
- enabling python integration

* Fri Oct 05 2007 Douglas E. Warner <silfreed@silfreed.net> 0.9.0-2
- enabling build for PPC64 (bug#247152)

* Wed Sep 26 2007 Douglas E. Warner <silfreed@silfreed.net> 0.9.0-1
- update to 0.9.0
- remove settings-include-workdir.patch
- updated man-install-share.patch to man-install-share-0.9.0.patch
- updated lib64-suffix.patch to lib64-suffix-0.9.0.patch
- enabled python to support msexport tool
- added Requires: grass to grass subpackage

* Tue Aug 28 2007 Douglas E. Warner <silfreed@silfreed.net> 0.8.1-13
- bump for expat 2.0 rebuild bug#195888

* Thu Aug 02 2007 Douglas E. Warner <silfreed@silfreed.net> 0.8.1-12
- updated License from GPL to GPLv2+

* Tue Jul 10 2007 Douglas E. Warner <silfreed@silfreed.net> 0.8.1-11
- allowing docs to be installed by qgis so they can be referenced by internal
  help system (bug#241403)
- updated lib64 patch (bug#247549) to try to get plugins found on x86_64

* Thu Jul 05 2007 Douglas E. Warner <silfreed@silfreed.net> 0.8.1-10
- updated lib64 patch for core and grass libraries

* Thu Jul 05 2007 Douglas E. Warner <silfreed@silfreed.net> 0.8.1-9
- updated lib64 patch

* Thu Jul 05 2007 Douglas E. Warner <silfreed@silfreed.net> 0.8.1-8
- adding ExcludeArch: ppc64 for bug#247152 (lrelease segfault)

* Thu Jul 05 2007 Douglas E. Warner <silfreed@silfreed.net> 0.8.1-7
- adding patch for lib64 support through lib_suffix

* Thu Jun 28 2007 Douglas E. Warner <silfreed@silfreed.net> 0.8.1-6
- fixed date of changelog entry for 0.8.1-5 from Wed Jun 27 2007 to
  Thu Jun 28 2007
- linking icon to included png instead of packaging it again

* Thu Jun 28 2007 Douglas E. Warner <silfreed@silfreed.net> 0.8.1-5
- adding comment on why grass is required in addition to grass-devel for BR
- fixing typo

* Wed Jun 27 2007 Douglas E. Warner <silfreed@silfreed.net> 0.8.1-4
- adding contributors to doc
- adding desktop file and icon

* Mon Jun 25 2007 Douglas E. Warner <silfreed@silfreed.net> 0.8.1-3
- updating detection of grass libraries to not use RPM call
- disabling building of -devel package due to shared libraries not being 
  versioned and having no other packages that compile against qgis
  (see bug #241403)
- removing chmod of test_export.py due to lack of python requirement
- removing msexport and share/python directory due to removal of python

* Fri Jun 22 2007 Douglas E. Warner <silfreed@silfreed.net> 0.8.1-2
- added BuildRequires: cmake
- updated build to use cmake macro and make verbose

* Mon Jun 19 2007 Douglas E. Warner <silfreed@silfreed.net> 0.8.1-1
- updating version
- removed BuildRequires: python-devel due to lack of PyQt4 bindings
- updated build for use of cmake instead of autotools
- added patch for setting WORKDIR in settings.pro file
- added patch for fixing install path of man pages
- updated library names

* Tue May 29 2007 Douglas E. Warner <silfreed@silfreed.net> 0.8.0-6
- adding BuildRequires bison, flex

* Tue May 29 2007 Douglas E. Warner <silfreed@silfreed.net> 0.8.0-5
- fixing more directory owernship (themes, themes-default)
- fixing qt4 hardcoded lib path
- removing Requires ldconfig
- adding BuildRequires sqlite-devel
- adding patch for supporting python 2.5 in configure script

* Sat May 26 2007 Douglas E. Warner <silfreed@silfreed.net> 0.8.0-4
- moving all BuildRequires to main section
- dropping use of makeinstall macro
- making sure directories are owned by this package
- removing *.a and *.la files
- disabled stripping of libraries and binaries to allow debuginfo package
  to be built
- fixing macros in changelog
- removing executable bits on source files

* Wed May 16 2007 Douglas E. Warner <silfreed@silfreed.net> 0.8.0-3
- fixing Requires statements for sub-packages

* Tue May 15 2007 Douglas E. Warner <silfreed@silfreed.net> 0.8.0-2
- added devel dependancy on qgis
- moved qgis-config to devel package
- moving doc directory
- removed zero-length NEWS doc
- added postin/postun ldconfig calls
- split packages up to reduce package size and split out dependancies
  grass, theme-nkids

* Mon May 14 2007 Douglas E. Warner <silfreed@silfreed.net> 0.8.0-1
- Initial RPM release.

