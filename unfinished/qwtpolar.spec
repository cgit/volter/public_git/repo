Name:      qwtpolar
Version:   0.1.0
Release:   1%{?dist}
Summary:   Display values on a polar coordinate system
Group:     System Environment/Libraries
License:   LGPLv2 with exceptions
URL:       http://qwtpolar.sourceforge.net/
Source0:   http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.bz2

# Introduces placeholders in Qmake's .pri and .pro file to be substituted later
Patch0:    %{name}_path.diff

BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildRequires: qt4-devel
BuildRequires: qwt-devel

%description
The QwtPolar library contains classes for displaying values on a polar
coordinate system. It is an add-on package for to the Qwt Library.

%package devel
Summary:        Development Libraries for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
This package contains the files necessary
to develop applications using QwtPolar.

%prep
%setup -q

%patch0 -p1 -b .path

# Verbose Makefile, set include- and lib paths as well as path for documentation
sed -i "/silent/d" qwtpolar.pri
sed -i "s\LIBPATH\ %{buildroot}/%{_libdir}\1" qwtpolar.pri
sed -i "s\HEADERPATH\ %{buildroot}/%{_includedir}/%{name}\1" qwtpolar.pri
sed -i "s\DOCPATH\ %{buildroot}/%{_docdir}/%{name}-%{version}\1" qwtpolar.pri
sed -i "s|/path/to/qwt-5.2/include|%{_includedir}/qwt|" qwtpolar.pri
sed -i "s|/path/to/qwt-5.2/lib|%{_libdir}|" qwtpolar.pri
# Add buildroot to installation path for Designer plug-in
sed -i "s|BUILDROOT|%{buildroot}|" designer/designer.pro
#sed -i "s|qwtpolar\\$\\$|\\$\\$|" designer/designer.pro
#sed -i "/qwtpolar\\$\\$/d" designer/designer.pro

chmod 644 COPYING

%build

qmake-qt4
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}
install -d %{buildroot}/%{_mandir}/man3/%{name}

#
#mv %{buildroot}%{_docdir}/%{name}/man/man3/* %{buildroot}/%{_mandir}/man3/%{name}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files 
%defattr(-,root,root,-)
%doc COPYING CHANGES
%{_libdir}/lib%{name}.so.*
%{_libdir}/qt4/plugins/designer/libqwt_polar_designer_plugin.so
%exclude %{_libdir}/lib%{name}.so
%exclude %{_includedir}/%{name}

%files devel
%defattr(-,root,root,-)
%{_mandir}/man3/%{name}
#%{_docdir}/%{name}-%{version}
%{_includedir}/%{name}
%{_libdir}/lib%{name}.so

%changelog
* Tue Feb 01 2011 Volker Fröhlich <volker27@gmx.at> 0.1.0-1
- Initial packaging for Fedora
